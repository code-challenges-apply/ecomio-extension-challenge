import uuid

from django.db import models


class Sustainability(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    information = models.TextField()
