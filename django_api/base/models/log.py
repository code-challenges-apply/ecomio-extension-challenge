import uuid
from enum import Enum

from django.db import models
from django.db.models import PROTECT
from base.models.sustainability import Sustainability


class OriginEnum(Enum):
    Kayak = "Kayak"
    Skyscanner = "Skyscanner"


class Log(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    origin = models.CharField(
        max_length=20, choices=[(e.name, e.value) for e in OriginEnum]
    )
    sustainability = models.ForeignKey(
        Sustainability, on_delete=PROTECT, related_name="logs"
    )
    created_at = models.DateTimeField(auto_now_add=True)
