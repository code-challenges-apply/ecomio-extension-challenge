from django.test import TestCase

# Create your tests here.

from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework.test import APIRequestFactory
from rest_framework import status
from .models.log import Log, OriginEnum
from .models.sustainability import Sustainability

from .views import LogViewSet


class LogViewSetTestCase(TestCase):
    def setUp(self):
        self.sustainability = Sustainability.objects.create(
            information="test information"
        )
        self.factory = APIRequestFactory()

    def test_perform_create(self):
        self.assertEqual(Log.objects.count(), 0)
        view = LogViewSet.as_view({"post": "create"})
        data = {
            "sustainability": self.sustainability.id,
            "origin": OriginEnum.Kayak.value,
        }
        request = self.factory.post(reverse("log-list"), data=data)
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Log.objects.count(), 1)


class SustainabilityViewSetTestCase(TestCase):
    def setUp(self):
        self.sustainability = Sustainability.objects.create(
            information="test information"
        )
        self.factory = APIRequestFactory()

    def test_retrieve_sustainability(self):
        response = self.client.get(reverse("sustainability-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data[0]["information"], self.sustainability.information
        )
