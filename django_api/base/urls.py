from rest_framework.routers import DefaultRouter

from base.views import SustainabilityViewSet, LogViewSet


urlpatterns = []

router = DefaultRouter()
router.register("sustainability", SustainabilityViewSet, basename="sustainability")
router.register("log", LogViewSet, basename="log")

urlpatterns += router.urls
