from django.contrib import admin

from base.models.log import Log
from base.models.sustainability import Sustainability

# Register your models here.


@admin.register(Log)
class LogAdmin(admin.ModelAdmin):
    readonly_fields = (
        "origin",
        "sustainability",
        "created_at",
    )


admin.site.register(Sustainability)
