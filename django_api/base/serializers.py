from rest_framework import serializers

from base.models.sustainability import Sustainability
from base.models.log import Log


class SustainabilityListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sustainability
        fields = "__all__"


class SustainabilityCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sustainability
        fields = ["information"]


class LogCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Log
        fields = ["origin", "sustainability"]
