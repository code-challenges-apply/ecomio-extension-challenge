from rest_framework.viewsets import ModelViewSet

from base.models.sustainability import Sustainability
from base.models.log import Log
from base.serializers import (
    SustainabilityListSerializer,
    SustainabilityCreateSerializer,
    LogCreateSerializer,
)


class SustainabilityViewSet(ModelViewSet):
    serializer_class = SustainabilityListSerializer
    # permission_classes = (IsAuthenticated,)
    http_method_names = ["get", "post"]
    queryset = Sustainability.objects.all()

    def get_serializer_class(self):
        if self.action in ["list", "retrieve"]:
            return self.serializer_class
        elif self.action == "create":
            return SustainabilityCreateSerializer
        else:
            return self.serializer_class


class LogViewSet(ModelViewSet):
    serializer_class = LogCreateSerializer
    # permission_classes = (IsAuthenticated,)
    http_method_names = ["post"]
    queryset = Log.objects.all()
