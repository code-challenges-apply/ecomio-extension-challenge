"""gunicorn WSGI server configuration."""
from multiprocessing import cpu_count
from os import environ


def max_workers():
    return cpu_count()


bind = "0.0.0.0:" + environ.get("PORT", "80")
daemon = False
max_requests = 100
timeout = 20
worker_class = "gevent"
workers = 1  # max_workers()
preload = True
errorlog = "-"
accesslog = "-"
loglevel = "info"
