// A TypeScript class representing a new DOM element that can be added to the page
export default class DOMElement {
  parent: HTMLElement; // The parent element for the new DOM element
  styles: string | string[]; // The CSS class(es) to be applied to the new DOM element
  element: HTMLElement; // The new DOM element being created

  // The constructor function for the class
  constructor(
    parent: HTMLElement, // The parent element for the new DOM element
    styles: string | string[], // The CSS class(es) to be applied to the new DOM element
    text?: Promise<string | string[]> | string | string[], // The text content to be added to the new DOM element
    link?: string, // An optional parameter that is not used in this code
  ) {
    // Set the parent and styles properties to the values passed in the constructor
    this.parent = parent;
    this.styles = styles;

    // Create a new 'div' element to represent the new DOM element
    this.element = document.createElement('div');

    // Add the specified styles to the new DOM element
    this.element = this.addStyles(this.styles);

    // If there is text content to add, call the addText method
    if (text) this.addText(text);
  }

  // A method to append the new DOM element to the parent element and return the new element
  appendToDom(): HTMLElement {
    const elementAlreadyAppended = this.parent.querySelector(`.${this.styles}`) !== null;
    if (!elementAlreadyAppended) this.parent.appendChild(this.element);
    return this.element;
  }

  // A method to prepend the new DOM element to the parent element and return the new element
  // If an element with the same CSS class already exists, it can be overwritten or returned
  prependToDom(overwrite?: boolean): HTMLElement {
    const existedElement = this.parent.querySelector(`:scope > .${this.styles}`);

    if (existedElement) {
      if (overwrite) this.parent.removeChild(existedElement);
      else return this.element;
    }
    this.parent.prepend(this.element);
    return this.element;
  }

  // A method to apply styles to the new DOM element
  addStyles(styles: string | string[]): HTMLElement {
    this.style(this.element, styles);
    return this.element;
  }

  // A private method to add text content to the new DOM element
  private async addText(text: Promise<string | string[]> | string | string[]) {
    // If the text content is a Promise, wait for it to resolve before continuing
    if (text instanceof Promise) text = await text;
    // If the text content is a string, add it to the new DOM element
    if (typeof text === 'string') {
      this.appendText(text);
    } else // If the text content is an array of strings, add each string to the new DOM element
      text.forEach((textElement) => {
        this.appendText(textElement);
      });
  }

  // A private method to add a single string of text content to the new DOM element
  private appendText(text: string) {
    const textElem = document.createElement('span'); // Create a new 'span' element
    const textNode = document.createTextNode(text); // Create a new text node with the specified text content
    textElem.appendChild(textNode); // Append the text node to the 'span' element
    this.element.appendChild(textElem); // Append the 'span' element to the new DOM element
  }

  // A private method to apply styles to a given HTML element
  private style(elem: HTMLElement, styles: string | string[]): HTMLElement {
    // Check if "styles" is a string
    if (typeof styles === 'string') {
      // If it's a string, add it as a class to the "elem"
      elem.classList.add(styles);
    } else {
      // If it's an array of strings, iterate over each string and add it as a class to the "elem"
      styles.forEach((style) => {
        elem.classList.add(style);
      });
    }
    // Return the "elem" with the added styles
    return elem;
  }
}
