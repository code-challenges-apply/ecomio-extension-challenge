
import DOMElement from "./DOMElement";

interface ResponseData {
  information: string;
  id: string
}

const API_ROOT = "http://localhost:8000/v1/api/"


const config = window.location.hostname.includes("kayak")? {
  inputElementsQuery: 'input[data-test-destination]',
  origin: "Kayak",
  styles: ["vvTc-item-value"],
  attribute: "data-test-destination"
 
} : {
  inputElementsQuery: 'input[id^="fsc-destination-search"]',
  origin: "Skyscanner",
  styles: ['BpkLabel_bpk-label__MDg2Y', 'App_form-label__ZjA3O'],
  attribute: "value"
  
}

console.log(config)

async function getSustainabilityData(query: string):Promise<ResponseData> {
  const response = await fetch(`${API_ROOT}sustainability/?q=${query}`);
  const data = await response.json();
  return data[0];
}

async function createLog(sustainability: string, origin: string){
  const response = await fetch(`${API_ROOT}log/`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({sustainability, origin})
  });
  const data = await response.json();
  return data[0];
}

// Find the input element with an id starting with "fsc-destination-search"
const inputElements = document.querySelectorAll<HTMLInputElement>( config.inputElementsQuery
  
);

// addlistener has issue with kayak

// // Define a function to handle changes to the input value
// async function handleInputChange(event: Event) {
//   const target = event.target as HTMLInputElement;
//   // If the input value is not empty, add a new div element after the input
//   if (target.getAttribute(config.attribute)!.trim() !== '') {
//     try {
//       const data = await getSustainabilityData(target.value);
//       console.log(data);
//       const newElement = new DOMElement(
//         target.parentNode! as HTMLElement,          // parent element
//         config.styles,  // styles as an array of strings
//         data.information  // text content as a string
//       );
  
//       newElement.prependToDom(true);
  
//       await createLog(data.id, config.origin);
//     } catch (error) {
//       console.error('Error fetching data:', error);
//     }
    
  
//   }
// }

// // Add the event listener to each input element found
// inputElements.forEach((inputElement) => {
//   inputElement.addEventListener('change', handleInputChange);
// });


// For kayak we should use observer format

var observer = new MutationObserver(function(mutations) {
  mutations.forEach(async function(mutation) {
    if (mutation.type === "attributes") {
      const target = mutation.target as HTMLInputElement;
  // If the input value is not empty, add a new div element after the input
  if (target.getAttribute(config.attribute)!.trim() !== '') {
    try {
      const data = await getSustainabilityData(target.value);
      console.log(data);
      const newElement = new DOMElement(
        target.parentNode! as HTMLElement,          // parent element
        config.styles,  // styles as an array of strings
        data.information  // text content as a string
      );
  
      newElement.prependToDom(true);
  
      await createLog(data.id, config.origin);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
    
  
  }
    }
    
    console.log(mutation.target);
  });
});



// Add the event listener to each input element found
inputElements.forEach((inputElement) => {
  observer.observe(inputElement, {
    attributes: true //configure it to listen to attribute changes
  });
});
